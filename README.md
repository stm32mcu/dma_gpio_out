# DMA_GPIO_Out

Pattern generator on GPIO port A using timer driven DMA from a memory buffer to the GPIO port A output data register (ODR).